############################
### Script by @JosemiGT. ###
############################

## Modules needed in this script.
import sys
import os
import json
import pandas as pd
import matplotlib.pyplot as plt

## Global constantes.
# PATH_CHOISE = ['--path', '--p']
# PLOT_CHOISE = ['--plotType', '--pT']
# SEPARATOR_CHOISE = ['--textSeparator', '--tS']
# OUTNAME_CHOISE = ['--outName', '--oN']
# PLOT_TITTLE = ['--plotTittle', '--pTit']

class PlotConfig(object):
    
    # Public properties
    pathData = ''
    extensionFile = ''
    plotType = "Default"
    textSeparator = ";"
    outName = 'plotGenerate.png'
    plotTittle = ''

    # Private properties
    __configF__ = ''

    def __init__(self, pathConfig = "plotConfig.json"):
        self.pathConfig = pathConfig
        self.__getConfig__()
        self.__readConfig__()

    def __getConfig__(self):
        with open(self.pathConfig) as configFile:
            self.__configF__ = json.load(configFile)
    
    def __readConfig__(self):
        if(self.__configF__['path']):
            self.pathData = self.__configF__['path']
            self.extensionFile = self.__configF__['path'].split(".")[-1]

        if(self.__configF__['plotType']):
            self.plotType = self.__configF__['plotType']

        if(self.__configF__['textSeparator']):
            self.textSeparator = self.__configF__['textSeparator']
        
        if(self.__configF__['outName']):
            self.outName = self.__configF__['outName']

        if(self.__configF__['plotTitle']):
            self.plotTitle = self.__configF__['plotTitle']

class PrintPlot(object):

    Data = ''

    def __init__(self, plotConfig):
        self.__plotConfig = plotConfig
        self.Data = self.readFile()
        self.plotData()

    def readFile(self):
        ext = str.upper(self.__plotConfig.extensionFile)
        if(ext == 'CSV'):
            return pd.read_csv(self.__plotConfig.pathData, sep=self.__plotConfig.textSeparator)
        elif(ext == 'XLS' or ext == 'XLSX'):
            return pd.read_excel(self.__plotConfig.pathData, sep=self.__plotConfig.textSeparator)
        else:
            return None

    def plotData(self):
        
        header = list(self.Data)  
        _legend = []

        for col in header:
            if col != header[0] and col != '':
                plt.plot(self.Data[header[0]],self.Data[col])
                _legend.append(col)

        plt.grid()

        # ejes
        plt.xlabel(header[0])
        plt.ylabel(header[1])

        # tittle
        if(self.__plotConfig.plotTitle):
            plt.title(self.__plotConfig.plotTitle, loc = "center")

        # legend
        plt.legend(_legend)

        plt.savefig(self.__plotConfig.outName)                                                                                   

## Check if exist config.json.
def checkConfig(path):
    return os.path.isfile(path)

def writeHelp():
    print('This script need a plotConfig.json, if you need a new file you can write JSON like argument in this program.')

def createConfigJson():
    plotConfig = {}
    plotConfig['path'] = ''
    plotConfig['outName'] = ''
    plotConfig['plotTitle'] = ''
    plotConfig['plotType'] = ''
    plotConfig['textSeparator'] = ''

    with open('plotConfig.json','w') as f:
        json.dump(plotConfig,f)

## Main execution programm
if __name__ == "__main__":

    if len(sys.argv) > 1 and str.upper(sys.argv[1]) == 'HELP':
        writeHelp()
    elif len(sys.argv) > 1 and str.upper(sys.argv[1]) == 'JSON': 
        createConfigJson()
    elif checkConfig('plotConfig.json'):
        config = PlotConfig()
        PrintPlot(config)
        print('finish plot generate.')
    elif len(sys.argv) > 1 and sys.argv[1].split('.')[-1] == 'json':
        config = PlotConfig(sys.argv[1])
        PrintPlot(config)
        print('finish plot generate.') 
    else:
        print('No config file in the path.')
